# boomset

## Project description
The project is using with Boomset API. That takes events and sessions to which it is depends.

Three variations were used as Vue.filter in **filters.js** for the date format.

In public folder **loading.svg** was use for vue-lazy.    

## Used plugins
* vue-router : for page redirect and set params are was get from data and manual.

* vue-lazyload : vue-lazy was used in case the event list was too long. And also triggered with scroll.

* moment : Three variations were used for the date format.  
    * **formatTime**: to see only time.
    * **formatDate**: to see only date with day string.
    * **formatDateTime**: to see date and time.


------ 

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
