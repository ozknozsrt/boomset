import Vue from 'vue'
import moment from 'moment'

Vue.filter('formatTime', function (value) {
    if (value) {
        return moment(String(value)).format('hh:mm')
    }
});
Vue.filter('formatDate', function (value) {
    if (value) {
        return moment(String(value)).format('dddd, DD MMM YYYY')
    }
});
Vue.filter('formatDateTime', function (value) {
    if (value) {
        return moment(String(value)).format('MM/DD/YYYY hh:mm')
    }
});
 