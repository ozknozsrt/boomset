import Home from './components/Home'
import EventDetail from './components/EventDetail'

export const routes = [
    { path: '/', name: 'Home', component: Home, meta: { title: 'Boomset' } },
    { path: '/detail/:id', name: 'EventDetail', component: EventDetail, meta: { title: 'Event Detail' } }
];