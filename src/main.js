import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import './filters'
import { routes } from './routes'
import VueLazyload from 'vue-lazyload'

Vue.config.productionTip = false

// Use plugins
Vue.use(VueRouter, VueLazyload)

// Router Settings
const router = new VueRouter({
  routes,
  mode: 'history'
})

// Lazyload Settings
Vue.use(VueLazyload, {
  preLoad: 1.3,
  attempt: 1,
  listenEvents: [ 'scroll' ] 
})

new Vue({
  render: h => h(App),
  router,
}).$mount('#app')
